package br.com.assesment.customerbase.assesmentcustomerbase.service;

import br.com.assesment.customerbase.assesmentcustomerbase.model.Customer;
import br.com.assesment.customerbase.assesmentcustomerbase.repository.CustomerRepository;
import br.com.assesment.customerbase.assesmentcustomerbase.services.CustomerService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private CustomerService customerService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldSaveCustomerSuccessfullyTest() {
        final Customer customer = new Customer();
        customer.setFirstName("John");
        customer.setLastName("Doe");
        customer.setBirthDate(LocalDate.of(1999, 1, 1));
        customer.setDocumentNumber("12345");
        customer.setEmail("john.doe@gmail.com");
        customer.setMobileNumber("47988888888");

        Customer result = customerService.save(customer);
        Assert.assertNotNull(result);
    }

}
