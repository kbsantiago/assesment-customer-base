package br.com.assesment.customerbase.assesmentcustomerbase.services;

import br.com.assesment.customerbase.assesmentcustomerbase.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import br.com.assesment.customerbase.assesmentcustomerbase.repository.CustomerRepository;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> getWithPagination(int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "firstName");
        List<Customer> customers = customerRepository.findAll(pageable).getContent();
        return customers;
    }

    public Customer findById(Long id) {
        try {
            Customer customer = customerRepository.findById(id).get();
            return customer;
        } catch(Exception ex) {
            return null;
        }
    }

    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

    public void delete(Long id) {
        customerRepository.deleteById(id);
    }
}
