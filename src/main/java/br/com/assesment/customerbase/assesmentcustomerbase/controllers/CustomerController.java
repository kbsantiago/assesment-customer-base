package br.com.assesment.customerbase.assesmentcustomerbase.controllers;

import br.com.assesment.customerbase.assesmentcustomerbase.model.Customer;
import br.com.assesment.customerbase.assesmentcustomerbase.services.dto.CustomerDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import br.com.assesment.customerbase.assesmentcustomerbase.services.CustomerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.stream.Collectors;

@Api(value = "Customer")
@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {

    private final CustomerService customerService;
    private final ModelMapper modelMapper;

    @Autowired
    public CustomerController(CustomerService customerService, ModelMapper modelMapper) {
        this.customerService = customerService;
        this.modelMapper = modelMapper;
    }

    @ApiOperation(value = "Return a pageable list of customers")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CustomerDto>> getCustomersPageable(@RequestParam int page, @RequestParam int size) {
        List<CustomerDto> customersDto = customerService.getWithPagination(page, size)
                .stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
        return ResponseEntity.ok(customersDto);
    }

    @ApiOperation(value = "Return single customer by id")
    @RequestMapping(method = RequestMethod.GET, path = "/{id}")
    public ResponseEntity<CustomerDto> getCustomerById(@PathVariable String id) {
        Customer customer = customerService.findById(Long.parseLong(id));
        if(customer == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(customer, CustomerDto.class));
    }

    @ApiOperation(value = "Add and return customer")
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CustomerDto> saveCustomer(@RequestBody CustomerDto customerDto) {
        Customer customer = customerService.save(modelMapper.map(customerDto, Customer.class));
        if(customer == null) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
        }
        return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(customer, CustomerDto.class));
    }

    @ApiOperation(value = "Update and return customer")
    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CustomerDto> updateCustomer(@RequestBody CustomerDto customerDto) {
        Customer customer = customerService.findById(customerDto.getId());
        if(customer == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        try {
            customer = customerService.save(modelMapper.map(customerDto, Customer.class));
            return ResponseEntity.status(HttpStatus.OK).body(modelMapper.map(customer, CustomerDto.class));
        } catch(Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @ApiOperation(value = "Delete customer by id")
    @RequestMapping(method = RequestMethod.DELETE, path = "/{id}")
    public ResponseEntity deleteCustomer(@PathVariable Long id) {
        Customer customer = customerService.findById(id);
        if(customer == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        try {
            customerService.delete(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    private CustomerDto convertToDto(Customer customer) {
        CustomerDto customerDto = modelMapper.map(customer, CustomerDto.class);
        return customerDto;
    }
}
