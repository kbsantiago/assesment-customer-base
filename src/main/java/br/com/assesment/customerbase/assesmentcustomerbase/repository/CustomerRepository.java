package br.com.assesment.customerbase.assesmentcustomerbase.repository;

import br.com.assesment.customerbase.assesmentcustomerbase.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface CustomerRepository extends JpaRepository<Customer, Long> {}
